'''
UKE 35
Oppsummering kode: hva ble gjennomgått denne uken?
'''

# eksempel som kombinerer aritmetikk, variabler, input, datatyper, f-string og print
#(oppsummerer det som ble gjennomgått uke 34)

# Nytt stoff under her:

# escape-tegn
print('Hei!\nNå er vi på ny linje.\nEnda en ny linje, med \'fnutter\' til og med!')
print()
print("Bruker jeg \" rundt strengen, slipper jeg å ha \\ foran ' for å skrive tegnet. Dette var merkelig.")
print()
 
# Pseudokode og kommentarer i koden
#Pseudokode er beskrivende  tekst, altså ikke ekte kode, som presist beskriver hva koden skal
#gjøre. Pseudokode og kommentarer i koden er forskjellig.

#sette flere variabler til samme verdi
a = b = c = 0

#lage flere variabler samtidig variabler
x, y, z = 1, 2, 3

#bytte verdi på to variabler
x, y = y, x

#aritmetikk, kortere skrivemåte:
# x = x+y:
x += y
# x = x - y
x -=y
# x = x*y
x *= y
# x = x/y
x /= y

#strenger
tekst = 'Min streng!'
# få tak i en bokstav, vi begynner på indeks (plass) 0:
forste = tekst[0] # gir 'M'
andre = tekst[1] # gir 'i'
tredje = tekst[2] # gir 'n'
siste =  tekst[-1] # gir '!'
nestSiste = tekst[-2] # gir 'g'
#legge sammen strenger ved casting
nyStreng = tekst+str(x) + str(y)
#legge sammen strenger ved f-string
enTilStreng = f'{tekst}{x}{y}'

#casting: int, float, str, bool
#int konverterer til heltall:
heltall = int('5')
desimaltall = float('5')
nyTekst = str(5+7)
sant = bool(1)
likeSant = bool(-5)
forsattSant = bool('tekst lenger enn 0 i lengde')
usant = bool(0)
usant = bool('')
usant = bool(0.0)

#sannhetsverdier
sjekk1 = 5>6 # gir False fordi 5 uttrykket vi skrev (5>6) stemmer IKKE
sjekk2 = 'Streng' == 'Streng' #gir True fordi uttrykket vi skrev (sjekker om de to er identiske) stemmer